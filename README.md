# Formation COST DATE

Rassemble les ressources nécessaire à la formation.

Vous trouverez le [cours](https://formation-r.frama.io/lecture/) et les [exercices](https://formation-r.frama.io/exercices/) en ligne.